'use strict';

const express = require ('express');
const router = express.Router();

const userMiddleware = require(__MIDDLEWARES + 'user');

//controllers

//create user
const UserController = require('./UserController');
router.post('/sign-up', userMiddleware.validateRegister, UserController.store);

module.exports = router;