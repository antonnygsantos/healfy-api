'use strict';

const User = require(__MODELS + 'User');
const bcrypt = require('bcryptjs');

module.exports = {
  async store(req, res) {
    const {name, email, password} = req.body;
    const userExists = await User.findOne({email: email});

    if (userExists) {
      return res.json({
        userExists,
        message: 'Already exist'
      });
    }

    const hash = await bcrypt.hash(password, 10);

    const user = await User.create({
      name,
      email,
      password: hash
    });

    return res.status(201).json(user);
  }
};