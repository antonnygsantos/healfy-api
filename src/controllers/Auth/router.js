'use strict';

const express = require('express');
const router = express.Router();

const User = require(__MODELS + 'User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

//controllers
router.post('/', async (req, res) => {
  const {
    email,
    password
  } = req.body;

  //Get user with that email
  const userExists = await User.findOne({
    email: email
  });

  //If user not exist
  if (!userExists) {
    return res.status(404).json({
      message: 'User with this email dont exist'
    });
  }

  //Comparing encrypt password
  bcrypt.compare(password, userExists.password, (error, result) => {
    //Throw any possible erroor
    if (error) {
      throw error;

      //If result isnt true 
    } else if (!result) {
      return res.status(401).json({
        message: 'Email or password is incorrect!'
      });

      //If it is true
    } else {

      //Generating token
      const token = jwt.sign({
          name: userExists.name,
          id: userExists._id
        },
        'SECRETKEY', {
          expiresIn: '7d'
        }
      );
      return res.status(200).json({
        userExists,
        token
      });
    }
  });
});

module.exports = router;