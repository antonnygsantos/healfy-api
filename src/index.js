'use strict';

const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const cors = require('cors');
require ('./config/mongo');

require(__dirname + '/globals.js')();

// SET UP PORT
const PORT = process.env.PORT || 3001;

// SET UP DEPENDENCIES 
app.use(bodyParser.json());
app.use(cors());

// SET UP USER ROUTER
const router = require('./routers');
app.use('/', router);


app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));