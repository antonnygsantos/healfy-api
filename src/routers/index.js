const express = require ('express');
const app = express();

const UserController = require (__CONTROLLERS + 'User/router');
app.use('/user', UserController);

const AuthController = require (__CONTROLLERS + 'Auth/router');
app.use('/auth', AuthController);

module.exports = app;
