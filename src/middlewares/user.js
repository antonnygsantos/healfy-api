module.exports = {
  validateRegister: (req, res, next) => {
    //username
    if (!req.body.name || req.body.name.length < 3){
      return res.status(400).send({
        message: 'Please enter a username with min. 3 chars'
      });
    }

    //password
    if (!req.body.password || req.body.password.length < 6){
      return res.status(400).send({
        message: 'Please enter a password with min. 6 chars'
      });
    }

    // if password repeat doesnt match
    if (!req.body.password_repeat || req.body.password != req.body.password_repeat){
      return res.status(400).send({
        message: 'Both passwords must match'
      });
    }

    next();
  }
};